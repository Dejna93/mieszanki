package sample;

import javafx.scene.control.TextField;
import org.apache.commons.math3.optim.linear.NonNegativeConstraint;
import org.apache.commons.math3.optimization.GoalType;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.optimization.linear.LinearConstraint;
import org.apache.commons.math3.optimization.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optimization.linear.SimplexSolver;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Damian on 2016-05-17.
 */
public class Mieszanki {

    private int iZmiennych;
    private int iWarunkow;

    private ArrayList<Double>funCelu = new ArrayList<>();
    private ArrayList <Equation>  equations = new ArrayList<>();

    public Mieszanki() {
    }

    public void setEntryData(String iloscZmiennych , String iloscWarunkow){

        this.iZmiennych = Integer.parseInt(iloscZmiennych);
        this.iWarunkow = Integer.parseInt(iloscWarunkow);

    }
    public void setFunCelu(ArrayList<TextField> textFields){

        for (int i=0 ; i < textFields.size() ; i++){
            funCelu.add(Double.parseDouble(textFields.get(i).getText()));
        }

    }
    public void setEquation(ArrayList<Double> equation, String operation , Double value ){

            equations.add( new Equation(equation,operation,value));
    }
    public void setEquation(ArrayList<Double> equation){
        this.equations.add(new Equation(equation));
    }


    public boolean validate(ArrayList<TextField> textFields){

        for(int i=0 ;i < textFields.size() ; i++){
            if(!textFields.get(i).getText().isEmpty()){
                funCelu.add( Double.parseDouble(textFields.get(i).getText()));
            }else {
                return false;
            }
        }
        return true;
    }

    public ArrayList<Double> solve(){

        ArrayList<Double>wynik = new ArrayList<>();

        double [] c = new double[iZmiennych];
        for(int i=0 ; i < funCelu.size(); i++){
            c[i] = funCelu.get(i);
        }

        LinearObjectiveFunction f = new LinearObjectiveFunction(c,0);
        Collection<LinearConstraint> constraints
                = new ArrayList<LinearConstraint>();
        for(Equation equation : equations){
            constraints.add(equation.getLinearConstraint());
        }
        SimplexSolver solver = new SimplexSolver();

        PointValuePair optSolution = new SimplexSolver()
                .optimize(
                    f,constraints,GoalType.MINIMIZE , true
                );

        for(int i =0 ; i < iZmiennych ;i++){
            wynik.add(optSolution.getPoint()[i]);
          //  System.out.println("Wynik x"+i + " " +x + " min " +z );
        }
        wynik.add(optSolution.getValue());

        return wynik;
    }

    public int getiZmiennych() {
        return iZmiennych;
    }

    public void setiZmiennych(int iZmiennych) {
        this.iZmiennych = iZmiennych;
    }

    public int getiWarunkow() {
        return iWarunkow;
    }

    public void setiWarunkow(int iWarunkow) {
        this.iWarunkow = iWarunkow;
    }

    public void setOperator(String operator) {
        this.equations.get(equations.size()-1).setOperator(operator);
    }

    public void setValue(double value) {
        this.equations.get(equations.size()-1).setValue(value);
    }

    @Override
    public String toString() {
        return "Mieszanki{" +
                "iZmiennych=" + iZmiennych +
                ", iWarunkow=" + iWarunkow +
                ", funCelu=" + funCelu +
                ", equations=" + equations +
                '}';
    }
}
