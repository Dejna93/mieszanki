package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Damian on 2016-05-17.
 */
public class VariableRow {
    ArrayList<Label> labels = new ArrayList<>();
    ArrayList<TextField> textFields = new ArrayList<>();
    TextField valuesFields;
    ComboBox comboBox;

    public VariableRow(int size) {

        for(int i =0 ; i < size ; i++){
            labels.add(new Label("Składnik "+(i+1)));
            TextField field =  new TextField();
            field.setPrefWidth(50);
            textFields.add(field);
            valuesFields = new TextField();
            valuesFields.setPrefWidth(50);

        }

    }
    public ArrayList<Label>getLabels(){
        return labels;
    }
    public ArrayList<TextField>getTextFields(){
        return textFields;
    }

    public GridPane add(GridPane pane, int x , int y ){

        for(int i =0 ; i < textFields.size(); i++){
            pane.add(labels.get(i),i,0);
            x = i;
        }

        for (int i = 0 ; i < textFields.size() ; i++){

            this.comboBox = new ComboBox(Controller.operations);
            comboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                    System.out.println("new value" + newValue);
                }
            });

            pane.add(textFields.get(i),i,y+1);

            if(i == textFields.size()-1 ){
                pane.add(comboBox,textFields.size(),y+1);
                pane.add(valuesFields,textFields.size()+1 , y+1);
            }
            x++ ;
        }
        return pane;
    }

    public ComboBox getComboBox(){
        return comboBox;
    }
    public TextField getValue(){
        return valuesFields;
    }
    public boolean validate(){
        for (TextField field : textFields){
            if(field.getText().isEmpty())
                return false;
        }
        return true;
    }
    public ArrayList<Double> toEquation(){
        ArrayList<Double>equation = new ArrayList<Double>();
            for(TextField field : textFields){
                equation.add(Double.parseDouble(field.getText()));
            }

        return equation;
    }
}
