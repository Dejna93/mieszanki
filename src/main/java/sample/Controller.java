package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Controller {

    private Stage stage;
    private GridPane pane;
    private Scene scene;

    private Mieszanki mieszanki =new Mieszanki();

    public static final ObservableList operations =
            FXCollections.observableArrayList();

    public Controller(Stage stage) {
        this.stage = stage;

        operations.addAll("<=","=" ,">=");
        System.out.println("Init GUI");
        init();
    }

    public void init(){
        pane = new GridPane();

        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);

        scene = new Scene(pane,800,600);

        stage.setTitle("Program do doboru mieszanek");
        stage.setScene(scene);
        stage.show();

        initFirstGui();

    }

    private void initFirstGui(){
        TextField [] texts = new TextField[2];
        Label [] labels = new Label[2];
        Button applyBtn = new Button("Ok");

        texts[0] = new TextField();
        texts[1] = new TextField();
        labels[0] = new Label("Ilość stopów");
        labels[1] = new Label("Ilość składników w stopach");

        applyBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println(texts[0].getText() +" " + texts[1].getText());

                if(!texts[0].getText().isEmpty() & !texts[1].getText().isEmpty()) {
                    mieszanki.setEntryData(texts[0].getText(), texts[1].getText());
                    pane.getChildren().clear();
                    initVariableGui();
                }
            }

        });



        for(int i =0 ; i < texts.length ; i++){
            pane.add(labels[i],i,0);
            pane.add(texts[i],i,1);
        }
        pane.add(applyBtn,2,1);
    }

    public VBox leftPanel(){
        VBox  vBox = new VBox();
        vBox.setPadding(new Insets(15,12,15,12));
        vBox.setSpacing(10);
        vBox.setStyle("-fx-background-color: dimgray");


        return vBox;
    }

    public HBox topPanel(){
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(15,15,15,15));
        hBox.setSpacing(10);
        hBox.setStyle("-fx-background-color: lightslategray");

        Label labelCost = new Label("Funkcja kosztów zakupu");
        Label labelCelu = new Label("min z(x)");

        labelCost.setPrefSize(200,20);
        labelCelu.setPrefSize(50,20);

        hBox.getChildren().addAll(labelCost,labelCelu);

        for(int i=0 ; i < mieszanki.getiZmiennych() ; i++){
            TextField field = new TextField();
            field.setPrefSize(100,20);
            field.setStyle("-fx-background-color: lightsteelblue");
            Label label = new Label("Koszt zakupu stopu "+(i+1));
            hBox.getChildren().addAll(label,field);
        }
    return hBox;
    }

    public GridPane centerPanel(){
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        gridPane.setPadding(new Insets(20,10,0,10));

        VariableRow [] rows = new VariableRow[mieszanki.getiWarunkow()];
        for(int i =0  ; i < mieszanki.getiWarunkow() ; i++) {
            rows[i] = new VariableRow(mieszanki.getiZmiennych());
            System.out.println(i);

            rows[i].add(gridPane,1,i);
        }

        Button applyBtn = new Button("Zatwierdź");

        applyBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                        int id = 0;

                        for (VariableRow row : rows) {
                            if (row.validate()) {
                                mieszanki.setEquation(row.toEquation());
                                mieszanki.setOperator((String)row.getComboBox().getSelectionModel().getSelectedItem());
                                mieszanki.setValue(Double.parseDouble(row.getValue().getText()));
                            }
                            id++;
                        }
                        ArrayList<Double> solved = mieszanki.solve();
                        String msg = "";
                        for (int i = 0; i < solved.size() - 1; i++) {
                            msg += "Składnik " + (i+1) + "= " + solved.get(i) + "\n";
                        }
                        msg += "Minimalna wartość " + solved.get(solved.size() - 1);
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Wyniki");
                        alert.setHeaderText("Mieszanki");
                        alert.setContentText(msg);
                        alert.show();

                    }

        });

        gridPane.add(applyBtn,mieszanki.getiZmiennych()+1,mieszanki.getiWarunkow()+1);

        return gridPane;
    }
    private void initVariableGui() {

        BorderPane borderPane = new BorderPane();
        VBox vBox = leftPanel();
        borderPane.setLeft(vBox);
        borderPane.setTop(topPanel());
        borderPane.setCenter(centerPanel());
        stage.setScene(new Scene(borderPane,800,600));

    }


    public void run() {
    }
}

















/*
        ArrayList<TextField> fCelu = new ArrayList<>();
        ArrayList<Label> lCelu = new ArrayList<>();
        //pane.setGridLinesVisible(true);
        pane.add( new Label("Funkcja kosztów zakupu"),0,0);
        pane.add( new Label("min z(x)"),0,2);

        for(int i = 0 ; i < mieszanki.getiZmiennych() ; i++){
            TextField filed= new TextField();
            filed.setPrefWidth(30);
            fCelu.add(filed);
            lCelu.add(new Label("Koszt zakupu stopu "+i));

            pane.add(lCelu.get(i),i+1 , 1);
            pane.add(fCelu.get(i),i+1 , 2);

        }

        ArrayList<Label> labels = new ArrayList<>();
        ArrayList<TextField> texts = new ArrayList<>();
        ArrayList<ComboBox> comboBoxes = new ArrayList<>();
        ArrayList<Label> valueLabeles = new ArrayList<>();
        ArrayList<TextField> valueFiled =  new ArrayList<>();

        pane.add(new Label("Zawartość składników w stopach"),0,4);

        VariableRow [] rows = new VariableRow[mieszanki.getiWarunkow()];
        for (int i=0 ; i < mieszanki.getiWarunkow() ; i++){
            rows[i] = new VariableRow(mieszanki.getiZmiennych());
            ComboBox combo = new ComboBox(operations);
            combo.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                    System.out.println("new value" + newValue);
                }
            });
            comboBoxes.add(combo);
            valueLabeles.add(new Label("Zawartość składników w wytopie"));
            TextField text = new TextField();
            text.setPrefWidth(30);
            valueFiled.add(text);
        }

        int iRow = 6;
        for(int i =0  ; i < mieszanki.getiWarunkow() ; i++) {

            pane.add(comboBoxes.get(i),mieszanki.getiZmiennych()+1,iRow+1);
            pane.add(valueLabeles.get(i),mieszanki.getiZmiennych() + 2  , iRow);
            pane.add(valueFiled.get(i),mieszanki.getiZmiennych()+2 , iRow+1);
            rows[i].add(pane, 1 , iRow );
            iRow+=2;
        }

        Button applyBtn = new Button("Zatwierdź");

       pane.add(applyBtn,0,iRow);
        applyBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(mieszanki.validate(fCelu) ){
                    int id =0 ;
                    System.out.println(comboBoxes.get(0).getSelectionModel().getSelectedItem());
                    for(VariableRow row : rows){
                        if(row.validate()){
                            mieszanki.setEquation(row.toEquation());
                            mieszanki.setOperator((String)comboBoxes.get(id).getSelectionModel().getSelectedItem());
                            mieszanki.setValue(Double.parseDouble(valueFiled.get(id).getText()));
                        }
                        id++;
                    }
                ArrayList<Double> solved = mieszanki.solve();
                    String msg = "";
                    for(int i=0 ; i < solved.size() -1 ; i++){
                        msg +="Składnik " + i +"= " + solved.get(i) +"\n";
                    }
                    msg +="Minimalna wartość " + solved.get(solved.size()-1);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Wyniki");
                    alert.setHeaderText("Mieszanki");
                    alert.setContentText(msg);
                    alert.show();
                    //mieszanki.solve();
                }
            }
        });
        
*/

