package sample;

import org.apache.commons.math3.optimization.linear.LinearConstraint;
import org.apache.commons.math3.optimization.linear.Relationship;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Damian on 2016-05-18.
 */
public class Equation {

    private double [] variables;
    private String operator;
    private double value;
    private int id;

    //----------------
    private Relationship relationship;


    public Equation(ArrayList<Double> var, String operator , Double value) {

        this.variables = new double[var.size()];

        for(int i=0 ; i < var.size() ; i++){
            variables[i] = var.get(i);
        }

        this.operator = operator;
        this.value = value;
    }

    public Equation(ArrayList<Double> var) {
        this.variables = new double[var.size()];

        for(int i=0 ; i < var.size() ; i++){
            variables[i] = var.get(i);
        }

    }


    public double[] getVariables() {
        return variables;
    }

    public void setVariables(double[] variables) {
        this.variables = variables;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        if(operator.equals("<=")){
            relationship = Relationship.LEQ;
        }else if(operator.equals(">=")){
            relationship = Relationship.GEQ;
        }
        else {
            relationship = Relationship.EQ;
        }
        this.operator = operator;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public LinearConstraint getLinearConstraint(){
        return new LinearConstraint(variables,relationship,value);
    }

    @Override
    public String toString() {
        return "Equation{" +
                "variables=" + Arrays.toString(variables) +
                ", operator='" + operator + '\'' +
                ", value=" + value +
                '}';
    }
}
